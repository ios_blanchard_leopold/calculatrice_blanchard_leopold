//
//  ViewController.swift
//  TP1
//
//  Created by mbds on 07/04/2021.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var containerData: UITextField!
    
    @IBOutlet weak var containerHistory: UITextView!
    
    
    var cacheNumber :String?
    var cacheOperator :Int?
    var newNumber :Int?
    var cacheHistory :String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        containerHistory.layer.borderWidth = 3
        containerHistory.layer.borderColor = UIColor.red.cgColor
    }
    
    
    @IBAction func showHiistory(_ sender: UIButton) {
        if(containerHistory.isHidden){
            containerHistory.isHidden = false
        }else{
            containerHistory.isHidden = true
        }
    }
    
    
    @IBAction func clearHistory(_ sender: UIButton) {
        cacheHistory = ""
        self.containerHistory.text = cacheHistory
    }
    
    
    @IBAction func addNumber(_ sender: UIButton) {
        if(newNumber == nil || newNumber == 0){
            containerData.text = (containerData.text ?? "")+String(sender.tag)
        }else{
            containerData.text = String(sender.tag)
            newNumber = 0
        }
    }
    
    func reset (){
        newNumber = 1
        cacheOperator = nil
        cacheNumber = nil
    }
    
    func addHistory(val1 :Double, val2 :Double, result :Double,
                operatorC :String){
        cacheHistory = (cacheHistory ?? "") + "\n" + " \(val1) " + operatorC + " \(val2) = \(result)"
        self.containerHistory.text = cacheHistory
    }
    
    @IBAction func butOperation(_ sender: UIButton) {
        newNumber = 1
        if(cacheOperator == nil ){
            cacheNumber = containerData.text
            cacheOperator = sender.tag
            
        }else {
            let n_cacheNumer: Double = Double(self.cacheNumber ?? "0") ?? 0
            let n_container : Double = Double(self.containerData.text ?? "0") ?? 0
            var result:Double? = 0
            
            switch cacheOperator {
            case -1:
                result  = n_cacheNumer + n_container
                addHistory(val1: n_cacheNumer, val2: n_container, result: result!, operatorC: "+")
            case -2:
                result  = n_cacheNumer - n_container
                addHistory(val1: n_cacheNumer, val2: n_container, result: result!, operatorC: "-")
            case -3 :
                result  = n_cacheNumer * n_container
                addHistory(val1: n_cacheNumer, val2: n_container, result: result!, operatorC: "*")
            case -4 :
                if(n_container == 0){
                    result = nil
                }else{
                    result = n_cacheNumer / n_container
                    addHistory(val1: n_cacheNumer, val2: n_container, result: result!, operatorC: "/")
                }
                
            case -5 :
                print("init")
                
            default:
                result  = n_cacheNumer + n_container
            }
            
          
            if(result == nil){
                self.cacheNumber = "0"
                self.containerData.text    = "infini"
            }else{
                self.cacheNumber = String(result!)
                self.containerData.text   = String(result!)
            }
            
            if(sender.tag == -5){
                print ("affectation nil")
               
                reset()
            }else{
                self.cacheOperator = sender.tag
            }
            
        }
    }
    
    
    @IBAction func butReset(_ sender: UIButton) {
        self.containerData.text  = ""
        reset()
    }
    
    
    
    @IBAction func butPoint(_ sender: UIButton) {
        let valContainer =  containerData.text ?? "0"
        if(newNumber == nil || newNumber == 0){
            if (!valContainer.contains(".")){
                if(containerData.text == nil || containerData.text == ""){
                    containerData.text = "0."
                }else{
                    containerData.text = (containerData.text ?? "")+"."
                }
            }
        }
    }
    
    
    @IBAction func plusOuMoinsEvent(_ sender: UIButton) {
        let valContainer =  containerData.text ?? "0"
        if(newNumber == nil || newNumber == 0){
            if (valContainer != "" && valContainer != "0" ){
                let valNumber :Double = ( Double(valContainer) ?? 0 ) * (Double(-1))
                containerData.text = String(valNumber)
            }
        }
        //containerData.text = String(2-3)
    }
    
    
    
    
}

